import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Sidebar from './sidebar';
import ShowTodo from './show'



export default class Add extends Component {

    constructor(props) {
        super(props);
        this.state = {
            todo:
            {
                title: '',
                description: ''
            },
            success:false,
            logout:false

        };
    }

    title(e) {
        this.state.todo.title = e.target.value;
        this.setState({
            todo: this.state.todo
        })
    }

    description(e) {
        this.state.todo.description = e.target.value;
        this.setState({
            todo: this.state.todo
        })
    }

    add(e) {
        this.setState({
            success:true
        })
    }

    logout(e) {
        this.setState({
            logout:true
        })
    }

    render() {
        const { success,logout } = this.state;
        if (success == true) {
            return <Redirect to = {{pathname:'/show', 
            state:{
                  todo:this.state.todo  
            }}}/>
        }
        
        if (logout == true) {
            return <Redirect to = {{pathname:'/signup', 
            state:{
                  todo:this.state.todo  
            }}}/>
        }
        return (
            <div>
                <div className='inline1'>
                    <Sidebar />
                </div>
                <div>
                    <button onClick={this.logout.bind(this)} type="button" className="logout btn btn-danger">Logout</button>
                </div>
                <div className='inline2'>
                    <div>
                        <h2 className='addtodoheader'>Add Task</h2>
                        <div className='addtodoheader form-group'>
                            <label for="email">Title</label>
                            <input type="text" required class="fbox form-control" id="title" placeholder="" value={this.state.todo.title} onChange={this.title.bind(this)} />
                        </div>
                        <div className="addtodoheader form-group">
                            <label for="email">Description</label>
                            <input type="text" required class="fbox form-control" id="des" placeholder="" value={this.state.todo.description} onChange={this.description.bind(this)} />
                        </div>
                        <button type="button" className="addtodobtn btn btn-dark" onClick={this.add.bind(this)}>Add</button>
                    </div>
                </div>
            </div>

        )
    }
}