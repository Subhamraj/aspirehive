import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import Sidebar from './sidebar';



export default class Calc extends Component {
    constructor(props) {
        super(props)
        this.state = {
            eq: '',
            logout:false
        }
    }

    clear(e) {
        this.setState({
            eq:''
        })
    }

    div(e) {
        var t = this.state.eq + '/'
        console.log(t)
        this.setState({
            eq:t
        })
        
    }

    seven(e) {
        var t = this.state.eq + '7'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    eight(e) {
        var t = this.state.eq + '8'
        console.log(t)
        this.setState({
            eq:t
        })   
    }

    nine(e) {
        var t = this.state.eq + '9'
        console.log(t)
        this.setState({
            eq:t
        })  
    }

    mul(e) {
        var t = this.state.eq + '*'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    four(e) {
        var t = this.state.eq + '4'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    five(e) {
        var t = this.state.eq + '5'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    six(e) {
        var t = this.state.eq + '6'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    minus(e) {
        var t = this.state.eq + '-'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    one(e) {
        var t = this.state.eq + '1'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    two(e) {
        var t = this.state.eq + '2'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    three(e) {
        var t = this.state.eq + '3'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    plus(e) {
        var t = this.state.eq + '+'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    dot(e) {
        var t = this.state.eq + '.'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    zero(e) {
        var t = this.state.eq + '0'
        console.log(t)
        this.setState({
            eq:t
        }) 
    }

    equal(e) {
        var ans = this.state.eq
        var r = eval(ans)
        this.setState({
            eq:r
        })
    }

    logout(e) {
        this.setState({
            logout:true
        })
    }

    render() {
        const { logout } = this.state;
        if (logout == true) {
            return <Redirect to = {{pathname:'/signup', 
            state:{
                  todo:this.state.todo  
            }}}/>
        }

        return (
            <div>
                <div className='inline1'>
                    <Sidebar />
                </div>
                <div>
                    <button onClick={this.logout.bind(this)} type="button" className="logout btn btn-danger">Logout</button>
                </div>

                <div className='inline2'>
                    <h2 className='addtodoheader'>Calculator</h2>

                    <div style={{ marginLeft: '6em', marginTop: '2em', width: '32em', height: '30em', background: 'Black', borderRadius: "20px" }}><div style={{ height: '1em' }}></div>
                        <div style={{ border: '3px solid black', width: '28em', textAlign: 'right', padding: '5px', background: 'white', marginLeft: '2em', height: '80px', borderRadius: "8px", fontFamily: 'Changa' }}>
                            <span style={{ fontSize: '44px' }}>{this.state.eq}</span>
                        </div>

                        <div>
                            <button onClick={this.clear.bind(this)} style={{ width: '20em', height: '3em', marginLeft: '2.5em', marginTop: '20px' }} type="button" class="btn btn-danger">Clear</button>
                            <button onClick={this.div.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">/</button>
                        </div>

                        <div>
                            <button onClick={this.seven.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '2.5em', marginTop: '20px' }} type="button" class="btn btn-danger">7</button>
                            <button onClick={this.eight.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">8</button>
                            <button onClick={this.nine.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">9</button>
                            <button onClick={this.mul.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">*</button>
                        </div>

                        <div>
                            <button onClick={this.four.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '2.5em', marginTop: '20px' }} type="button" class="btn btn-danger">4</button>
                            <button onClick={this.five.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">5</button>
                            <button onClick={this.six.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">6</button>
                            <button onClick={this.minus.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">-</button>
                        </div>

                        <div>
                            <button onClick={this.one.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '2.5em', marginTop: '20px' }} type="button" class="btn btn-danger">1</button>
                            <button onClick={this.two.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">2</button>
                            <button onClick={this.three.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">3</button>
                            <button onClick={this.plus.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">+</button>
                        </div>

                        <div>
                            <button onClick={this.dot.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '2.5em', marginTop: '20px' }} type="button" class="btn btn-danger">.</button>
                            <button onClick={this.zero.bind(this)} style={{ width: '6em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-danger">0</button>
                            <button onClick={this.equal.bind(this)} style={{ width: '13em', height: '3em', marginLeft: '1em', marginTop: '20px' }} type="button" class="btn btn-success">=</button>
                        </div>

                    </div>
                </div>

            </div>
        )
    }
}