import React, { Component } from 'react';
import{ BrowserRouter as Router,Route, Redirect } from "react-router-dom";

export default class Home extends Component {

  state = {
    redirect: false
  }

  evaluate = () => {
    this.setState({
      redirect: true
    })
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/signup' />
    }
  }

  render() {
    return (
      <div>
        {this.renderRedirect()}
        <button type="button" className="homebtn btn btn-dark" onClick={this.evaluate.bind(this)}>Evaluate Test</button>
      </div>
    )
  }
}