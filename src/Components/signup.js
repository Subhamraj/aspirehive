import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

export default class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            registerUser: 
                {
                    email: '',
                    fullname: '',
                    password: ''
                },
                success: false
        }
    }

    password(e) {
        this.state.registerUser.password = e.target.value;
        this.setState({
            registerUser: this.state.registerUser
        })
    }

    email(e) {
        this.state.registerUser.email = e.target.value;
        this.setState({
            registerUser: this.state.registerUser
        })
    }

    fullname(e) {
        this.state.registerUser.fullname = e.target.value;
        this.setState({
            registerUser: this.state.registerUser
        })
    }

    signup(e) {
        const { email,fullname,password } = this.state.registerUser;

        if(email == '') {
            alert('Email name can\'t be blank')
        }

        if(fullname == '') {
            alert('Fullname name can\'t be blank')
        }

        if(password == '') {
            alert('Password name can\'t be blank')
        }

        else {
            this.setState({
                success:true
            }, console.log(this.state.registerUser))
        }
        }


    render() {

        const { success } = this.state;
        if (success == true) {
            return <Redirect to = {{pathname:'/signin', 
            state:{
                  User:this.state.registerUser  
            }}}/>
        }

        return (
            <div className='signupcard'>
                <h2>Signup</h2>
                <div className='signupbox'>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" required value={this.state.registerUser.email} class="fbox form-control" onChange={this.email.bind(this)} id="email" placeholder="Enter email" />
                </div>
                <div class="form-group">
                    <label for="name">FullName</label>
                    <input type="text" required value={this.state.registerUser.fullname} class="fbox form-control" onChange={this.fullname.bind(this)} id="name" placeholder="Enter Username" />
                </div>
                <div class="form-group">
                    <label for="name">Password</label>
                    <input type="password" required value={this.state.registerUser.password} class="fbox form-control" onChange={this.password.bind(this)} id="name" placeholder="Enter Username" />
                </div>
                <button type="button" className="subt btn btn-dark" onClick={this.signup.bind(this)}>Signup</button>
                </div>
            </div>
        )
    }
}