import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Logo from '../assets/download.png';

export default class Sidebar extends Component {


    render() {
        return (
            <div className='sidebar'>
                <img className='logo' src={Logo} alt='Logo' />
                <ul class="sideul nav flex-column">
                <Link to={{ pathname: "/show", state: { todo: "" }}}> <li class="nav-link active"> To Do List </li> </Link>
                <Link to={{ pathname: "/calc" }}> <li class="nav-link active"> Calculator </li> </Link>
                </ul>
            </div>
        )
    }
}

