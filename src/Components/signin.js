import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

export default class Signin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            User: this.props.location.state.User || {} ,
            validateUser: {
                email: '',
                password: ''
            },
            success: false
        }
    }

    password(e) {
        this.state.validateUser.password = e.target.value;
        this.setState({
            registerUser: this.state.validateUser
        })
    }

    email(e) {
        this.state.validateUser.email = e.target.value;
        this.setState({
            registerUser: this.state.validateUser
        })
    }

    signin(e) {
        if (this.state.validateUser.email == this.state.User.email && this.state.validateUser.password == this.state.User.password) {
            console.log("Here");
            this.setState({
                success: true
            })
        }
        else {
            alert("Invalid Credentials")
        }
    }

    render() {
        const { success } = this.state;
        if (success == true) {
            return <Redirect to={{ pathname: '/show' , state: { todo: "" } }} />
        }
        return (
            <div>
                <div className='signupcard'>
                    <h2>Login</h2>
                    <div className='signupbox'>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required value={this.state.validateUser.email} class="fbox form-control" onChange={this.email.bind(this)} id="email" placeholder="Enter email" />
                        </div>
                        <div class="form-group">
                            <label for="name">Password</label>
                            <input type="password" required value={this.state.validateUser.password} class="fbox form-control" onChange={this.password.bind(this)} id="name" placeholder="Enter Username" />
                        </div>
                        <button type="button" className="subt btn btn-dark" onClick={this.signin.bind(this)}>Login</button>
                    </div>
                </div>
            </div>
        )
    }
}