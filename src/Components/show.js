  import React, { Component } from 'react';
  import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
  import Sidebar from './sidebar';



  export default class ShowTodo extends Component {

    constructor(props) {
      super(props);
      this.state = {
        todos: [
          {
            title: '1',
            description: '1'
          }
        ],
        addtodoflag: false,
        // todo: this.props.location.state.todo || {},
        remove: false,
        logout:false
      }
    }

    componentWillMount() {
      if (this.props.location.state.todo == "") {
        console.log("no new todos added");
      } else {
        this.state.todos.push(this.props.location.state.todo);
        this.setState({
          todos: this.state.todos
        });
      }
    }

    add(e) {
      this.setState({
        addtodoflag: true
      })
    }

    delete(index) {
    console.log(index);
    this.state.todos.splice(index, 1);
        this.setState({
          todos: this.state.todos
        });
    }

    logout(e) {
      this.setState({
          logout:true
      })
  }

    render() {
      const { addtodoflag,logout } = this.state;
        if (addtodoflag == true) {
            return <Redirect to={{ pathname: '/add' }} />
        }

        if (logout == true) {
          return <Redirect to = {{pathname:'/signup', 
          state:{
                todo:this.state.todo  
          }}}/>
      }

      const todos = this.state.todos.map((t, index) => (
        <li className='task' key={index} {...t} > {t.title} <span onClick={this.delete.bind(this , index)} className='x'>X</span> </li>
      ));


      return (
        <div>
          <div className='inline1'>
            <Sidebar />
          </div>
          <div>
            <button onClick={this.logout.bind(this)} type="button" className="logout btn btn-danger">Logout</button>
          </div>
          <div className='inline2'>
            <button type="button" className="addtodobtn btn btn-primary"  onClick={this.add.bind(this)}>Add Todo</button>
            <h2 className='addtodoheader'>To-Do List</h2>

            <ul className='list'>
                    <div> {todos} </div>
                </ul>

          </div>
        </div>
      )
    }
  }