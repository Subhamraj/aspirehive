import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Home from './Components/homepage';
import Signin from './Components/signin';
import Signup from './Components/signup';
import ShowTodo from './Components/show';
import Add from './Components/add';
import Calc from './Components/calculator';

import './App.css';


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/signin' component={Signin} />
        <Route exact path='/show' component={ShowTodo} />
        <Route exact path='/add' component={Add} />
        <Route exact path='/calc' component={Calc} />
      </Switch>
    </Router>
  );
}

export default App;
